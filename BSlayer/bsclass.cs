﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using DAlayer;
using CNMlayer;

namespace BSlayer
{
    public class bsclass
    {
        daclass da = new daclass();
        public DataSet insertperson()
        {
            return da.insertperson();
        }
        public void insertpersondtls(cnmclass som)
        {
            da.inserpersondtl(som);
        }
        public void updateperson(cnmclass som)
        {
            da.updateperson(som);
        }
        public void deletepersondtl(cnmclass gowd)
        {
            da.deleteperson(gowd);
        }
    }
}

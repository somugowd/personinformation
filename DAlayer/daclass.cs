﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using CNMlayer;

namespace DAlayer
{
    public class daclass
    {
        SqlConnection conn = new SqlConnection("Data Source=DESKTOP-FATGQMV;Initial Catalog=mvcrud;Integrated Security=True");
        public DataSet insertperson()
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = " select * from personinfo";
            cmd.CommandType = CommandType.Text;
            cmd.Connection = conn;
            SqlDataAdapter dt = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            conn.Open();
            dt.Fill(ds, "test");
            conn.Close();
            return ds;

        }
        public void inserpersondtl(cnmclass swty)
        {
            SqlConnection conn = new SqlConnection("Data Source=DESKTOP-FATGQMV;Initial Catalog=mvcrud;Integrated Security=True");
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "insertperson";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = conn;
            cmd.Parameters.AddWithValue("@Name", swty.Name);
            cmd.Parameters.AddWithValue("@DOB", swty.DOB);
            cmd.Parameters.AddWithValue("@Gender", swty.Gender);
            cmd.Parameters.AddWithValue("@Address", swty.Address);
            cmd.Parameters.AddWithValue("@State", swty.State);
            cmd.Parameters.AddWithValue("@Country", swty.Country);
            cmd.Parameters.AddWithValue("@PhoneNumber", swty.PhoneNumber);
            cmd.Parameters.AddWithValue("@Mail", swty.Mail);
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
        }
        public void updateperson(cnmclass swty)
        {
            SqlConnection conn = new SqlConnection("Data Source=DESKTOP-FATGQMV;Initial Catalog=mvcrud;Integrated Security=True");
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "updateperson";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = conn;
            cmd.Parameters.AddWithValue("@Id", swty.Id);
            cmd.Parameters.AddWithValue("@Name", swty.Name);
            cmd.Parameters.AddWithValue("@DOB", swty.DOB);
            cmd.Parameters.AddWithValue("@Gender", swty.Gender);
            cmd.Parameters.AddWithValue("@Address", swty.Address);
            cmd.Parameters.AddWithValue("@State", swty.State);
            cmd.Parameters.AddWithValue("@Country", swty.Country);
            cmd.Parameters.AddWithValue("@PhoneNumber", swty.PhoneNumber);
            cmd.Parameters.AddWithValue("@Mail", swty.Mail);
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
        }
        public void deleteperson(cnmclass swty)
        {
            SqlConnection conn = new SqlConnection("Data Source=DESKTOP-FATGQMV;Initial Catalog=mvcrud;Integrated Security=True");
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "deleteperson";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = conn;
            cmd.Parameters.AddWithValue("@Id", swty.Id);
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
        }
    }


}

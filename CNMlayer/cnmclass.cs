﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CNMlayer
{
    public class cnmclass
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Name Required field")]
        public string Name { get; set; }
        public DateTime DOB { get; set; }
        public string Gender{ get; set; }
        public string Address{ get; set; }
        public string State{ get; set; }
        public string Country{ get; set; }
        public string PhoneNumber{ get; set; }
        public string Mail{ get; set; }
    }
}
